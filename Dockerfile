FROM node:latest
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y python-pip python-dev
RUN apt-get install -y git
RUN npm install -g less

# Install Maven and what not
RUN apt-get install -y maven2 openjdk-7-jdk

ENV PATH /usr/local/bin/lessc:$PATH
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
