# Software Metrics Framework

This repository provides a framework for analyzing software complexity metrics in a reproducible, robust, and scaleable manner.
Specifically, this framework provides tools to:

 * Download Java projects which can easily be compiled for analysis
 * Run metrics against compiled Java projects
 * Generate graphs and other visualization aides
 * View results as they become available; not simply waiting for long running processes to complete

## Setup

Ubuntu setup should be more less restricted to the following:

```
sudo apt update
sudo apt upgrade
sudo apt install -y postgresql-all
sudo apt install -y python-virtualenv2
sudo apt install -y python-dev
sudo apt install -y maven default-jdk
```

If you are operating in a production host, do not run the below command.
Instead, configure a new database and specify the username and password in the file
`local_settings.py`.
This command makes it so you can connect to the postgresql server without authentication
from a local machine.

```
sudo cp /etc/postgresql/9.*/main/pg_hba.conf ./pg_hba.conf.bak
sudo sed -i 's/peer$/trust/; s/md5$/trust/; s/ident$/trust/' /etc/postgresql/9.*/main/pg_hba.conf
sudo su postgres -c "createdb smf"
```

Configure Django to use the database; the below command should work in most cases.
It will create a `local_settings.py` file, which you can modify if you need a more advanced
configuration.

```
cat <<CONFIG > local_settings.py
DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql',
         'NAME': 'smf',
         'USER': 'postgres',
         'HOST': 'localhost',
         'PORT': 5432,
    }
}

# GIT_REPO_BASE = "/media/charles/Research/git_repos" # Specify location of Git repos
CONFIG
```

Install the required Python libraries.
Although not strictly required, we recommend the use of Python virtual environments
to control the versions of libraries and dependencies; this makes it easier to keep
things working as time progresses.

This command should be run anytime you begin development

```
. ./activate
```

Lastly, trying running Django to see if everything is working

```
python manage.py shell
```

### Fetching the corpus

Tools to rebuild the corpus are located in the corpus folder; namely fetch_apache_projects.py.
However, to save time and make things consistent, we recommend using a pre-sanitized
corpus available in the corpus branch of this repository.

```
git show corpus:corpus.txt | python manage.py dbshell
```

If this commaned completes without error, the corpus should be loaded and ready to go.
You can verify this by starting the web interface and selecting several projects;
they should have a number of versions and issues.

```
python manage.py runserver
```

Then go to http://127.0.0.1:8000/

### First run

We do not include the git repos in the corpus, as they are relatively cheap to download
but would require a lot of space to store.
If you have trouble getting access to these files, please reach out to us and we'll
make the backups we have available.

```
python manage.py fetch_project --only_versions --all
```

Note that this wil take some time to run.
Please be patient.

Once all the projects are downloaded, you can start running metrics.
For example, to run the IC-WMC metric we can execute:

```
python manage.py run_metric --all ./metrics/interactive_complexity/wmc.py
```

This will take some time to run, but when it is complete, you should be able to
see IC-WMC values in the projects using the web UI.

Note that if you've read the SMF paper published in arXiv, we have completed the
first few stages: compilation and metadata.
All that remains is data analysis.

## Usage

As a rule of thumb, all scripts contributed to this project must utilize the Python argparser framework to provide help.
This means that every script should respect the '-h' flag; for example:

```
chathaway@blaze ~/programming/data_gathering (git)-[master] % python manage.py run_metric -h
usage: manage.py run_metric [-h] [--version] [-v {0,1,2,3}]
                            [--settings SETTINGS] [--pythonpath PYTHONPATH]
                            [--traceback] [--no-color]
                            [--git-repo-base REPO_BASE] [--no-compile]
                            [--project PROJECT]
                            shell_script [shell_script ...]

Runs the script specified as an argument, passing in the path to MVN project
as an argument The script should can report metric values by printing to
STDOUT like this: #>> METRIC_NAME=METRIC_VALUE The output is grepped for #>>,
then the values split and stored If the script runs longer than --timeout
(default, 5 minutes) it gets killed

positional arguments:
  shell_script          A script to run

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -v {0,1,2,3}, --verbosity {0,1,2,3}
                        Verbosity level; 0=minimal output, 1=normal output,
                        2=verbose output, 3=very verbose output
  --settings SETTINGS   The Python path to a settings module, e.g.
                        "myproject.settings.main". If this isn't provided, the
                        DJANGO_SETTINGS_MODULE environment variable will be
                        used.
  --pythonpath PYTHONPATH
                        A directory to add to the Python path, e.g.
                        "/home/djangoprojects/myproject".
  --traceback           Raise on CommandError exceptions
  --no-color            Don't colorize the command output.
  --git-repo-base REPO_BASE
                        Where to store the Git repos that must be downloaded
  --no-compile          Do not compile projects before calling the metrics
  --project PROJECT     Which project to run on; if omitted, runs on all
                        projects
```

We give a brief summary of the most important scripts, and directory layout, below.

## manage.py

This is the most important script.
It is a Django generated script which gives us access to all the management commands we use for most of the work; namely fetch_project and run_metric.
It is also how we perform database maintence/backups, manage the web interface, and control access to the ability to add repositories.

### Web Interface

The web interface is very bare bones.
If you run `python manage.py runserver`, it can be accessed
at http://127.0.0.1:8000/

IMPORTANT; there is a another section of the web interface at http://127.0.0.1:8000/admin.
This can be used to add projects to the pipeline for analysis.
Please create a user after the database is loaded to access it with `python manage.py createsuperuser`

## metrics/

This folder contains all the metrics that can be run.
Each metric suite should have a README.md file describing the metric, along with configuration instructions.

## local_settings.py

This file is used to augment the Django `settings.py` file.
Any settings put into this file override the default settings, which provides a way of making system-specific modifications without requiring source changes.

This file is not kept in versions control.

## Dockerfile, docker-compose.yml, .dockerignore

These files are used to run the framework within Docker.
Although this is useful in theory, we have found that docker presents too many performance
bottlenecks to be useful.

## analysis/

There are a number of scripts in this folder which can be used to do analysis on the results of the metrics.
For example, `calculate_pearsons.py` will output the pearsons correlation of the metrics in relation to the bug counts.

## corpus/

This folder contains scripts related to managing the corpus.
For example, `fetch_apache_projects.py` will scan the Apache homepage for projects and determine which of those we can use for analysis.
Note that projects acquired this way should not be considered vetted, and may have issues.

# Disclaimer

There will be problems.
Doing an analysis of this nature is very complicated, with each project introducing caveats and strangeness of it's own.
While we attempted to limit the scope of these problems by using Maven to control lifecycle activities, one can expect that not everything will work perfectly.
We do our best to keep going, even if one particular project really screws things up, which will result in a small loss of data.
For example, several alpha builds of projects fail to build; for many metrics, this means we can't analyze them.
We simply continue when this happens, meaning we lose one point of data in the analysis.

If this is something you feel strongly about, feel free to fix those projects.
You can manually specify versions in the web interface, which enables one to fix a problem and "replace" the old version with their changes.
Note that if you do this, DO NOT RUN fetch_projects as it will override your changes.

Occasionally there will be a system configuration problem.
The error messages emitted by SMF, mostly Python stack traces, can be used to help identify and troubleshoot most of these problems.
