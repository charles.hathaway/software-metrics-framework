#!/usr/bin/env python
""" Calculates and outputs pearson correlations for metrics
"""
import argparse
import doctest
import os
import sys
import logging

import os
import sys
sys.path.insert(0, ".")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data_gathering.settings")


import django
django.setup()
from django.conf import settings
from data_gathering.models import *

from matplotlib import pyplot
from scipy.stats.stats import pearsonr
from scipy.stats import norm
import matplotlib.patches as mpatches

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Generates correlations of intervals (release) of metrics versus bug count')
    parser.add_argument('project_name', nargs='*', type=str)
    parser.add_argument('--all', action='store_true', dest='all',
        default=False, help='Process all projects')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                action="store_true")
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    options = vars(parser.parse_args())
    if options['verbose']:
        logging.basicConfig(level=logging.DEBUG)
    if options["test"] == True:
        doctest.testmod()
        exit(0)
    sys.path.insert(0, BASE_DIR)

    projects = options['project_name']
    if options['all']:
        projects = [p.name for p in Project.objects.all()]
    sys.stdout.write('Calculating Pearson correlations for projects: %s\n' % (', '.join(projects)))
    total_values = {}
    p_values = {}
    number_of_bugs = {}
    number_of_releases = {}
    project_names = {}
    metrics = ["IC-DIT", "IC-WMC", "IC-LCOM1", "IC-RFC", "IC-NOC"]
    for project_name in projects:
        project = Project.objects.get(name=project_name)
        versions = [v for v in project.projectversion_set.filter(git_sha__isnull=False)] # Rely on natural ordering ("id")
        for metric in metrics:
            logging.info("Doing math for %s metric %s" % (project_name, metric))
            # Fetch all metrics for this project from the database
            unordered_metric_values = MetricValue.objects.filter(project=project, name=metric).all()
            bug_count_values = []
            metric_values = []
            # Get versions and number of bugs in that version from the database
            for v in versions:
                bug_count = v.projectissue_set.count()
                metric_value = None
                for mv in unordered_metric_values:
                    if mv.git_sha == v.git_sha:
                        metric_value = mv.value
                        break
                if metric_value == None or bug_count == 0:
                    continue
                metric_values += [float(metric_value)]
                bug_count_values += [bug_count]
            if len(metric_values) > 10 and len(metric_values) == len(bug_count_values):
                if metric not in total_values:
                    total_values[metric] = [[], []]
                    p_values[metric] = []
                    number_of_bugs[metric] = []
                    number_of_releases[metric] = []
                    project_names[metric] = []
                result = pearsonr(metric_values, bug_count_values)
                total_values[metric][0] += metric_values
                total_values[metric][1] += bug_count_values
                p_values[metric] += [result[1]]
                number_of_bugs[metric] += [sum(bug_count_values)]
                number_of_releases[metric] += [len(versions)]
                project_names[metric] += [project_name]
                print "%s & %s & %s & %s \\\\" % (project_name, metric, round(result[0], 2), round(result[1], 2))
    print total_values
    for key, value in total_values.iteritems():
        print "Total results for %s" % key
        result = pearsonr(value[0], value[1])
        print result
        pdf = norm.pdf(p_values[key])
        pyplot.scatter(number_of_bugs[key], p_values[key])
        for i, value in enumerate(number_of_bugs[key]):
            pyplot.annotate(project_names[key][i], [number_of_bugs[key][i], p_values[key][i]])
        pyplot.savefig("%s-number_of_bugs_vs_pval.png" % key)
        pyplot.close()
        pyplot.scatter(number_of_releases[key], p_values[key])
        for i, value in enumerate(number_of_releases[key]):
            pyplot.annotate(project_names[key][i], [number_of_releases[key][i], p_values[key][i]])
        pyplot.savefig("%s-number_of_releases_vs_pval.png" % key)
        pyplot.close()
        values = [float(number_of_releases[key][i])/(number_of_bugs[key][i]) for i, value in enumerate(number_of_bugs[key])]
        fig, ax = pyplot.subplots()
        ax.scatter(values, p_values[key])
        for i, value in enumerate(number_of_releases[key]):
            ax.annotate(project_names[key][i], [values[i], p_values[key][i]])
        ax.set_ylabel("Two Tailed p-value")
        ax.set_xlabel("Number of releases/Number of bugs")
        pyplot.savefig("%s-random_vs_pval.png" % key)
        pyplot.close()
