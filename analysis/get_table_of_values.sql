select
    t1.name
    , round(t2.value, 2) as "IC-DIT"
    , round(t3.value, 2) as "IC-WMC"
    , round(t4.value, 2) as "IC-RFC"
    , round(t5.project_id, 2) as "IC-LCOM1"
    , (t6.value) as "Bug Count"
from (select id, name from data_gathering_project group by id, name) as t1
left join (select project_id, median(value) as value from data_gathering_metricvalue where name = 'IC-DIT' group by project_id) as t2
on (t1.id = t2.project_id)
left join (select project_id, median(value) as value  from data_gathering_metricvalue where name = 'IC-WMC' group by project_id) as t3
on (t1.id = t3.project_id)
left join (select project_id, median(value) as value from data_gathering_metricvalue where name = 'IC-RFC' group by project_id) as t4
on (t1.id = t4.project_id)
left join (select project_id, median(value) as value  from data_gathering_metricvalue where name = 'IC-LCOM1' group by project_id) as t5
on (t1.id = t5.project_id)
left join (
    select project_id, count(*) as value
    from data_gathering_projectversion as t1
    inner join data_gathering_projectissue_fix_version as t2
    on (t1.id = t2.projectversion_id)
    inner join data_gathering_projectissue as t3
    on (t3.id = t2.projectissue_id)
    where t3.issue_type = 'Bug'
    group by project_id) as t6
on (t1.id = t6.project_id);
