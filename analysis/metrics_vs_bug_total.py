#!/usr/bin/env python
""" Downloads a list of projects from the git.apache.org, checks for their
        presence in JIRA, downloads the Git repo in checks to see if it contains
        a pom.xml file. If all of these conditions are met, and the project isn't
        already in the database, inserts the project into the database for analysis
"""
import argparse
import doctest
import os
import sys
import logging

import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, BASE_DIR)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data_gathering.settings")


import django
django.setup()
from django.conf import settings
from data_gathering.models import *

from matplotlib import pyplot
import matplotlib.patches as mpatches

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Generates charts of intervals (release) versus bug count')
    parser.add_argument('project_name', nargs='*', type=str)
    parser.add_argument('--all', action='store_true', dest='all',
        default=False, help='Process all projects')
    parser.add_argument('--metric', dest='metric', action='append',
        help='Which metric to generate graphs for')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--output', type=str, dest='output',
        default="./", help='Where to store the generated charts')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                action="store_true")
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    options = vars(parser.parse_args())
    if options['metric'] == None:
        options['metric'] = []
    if options['verbose']:
        logging.basicConfig(level=logging.DEBUG)
    if options["test"] == True:
        doctest.testmod()
        exit(0)
    sys.path.insert(0, BASE_DIR)

    projects = options['project_name']
    if options['all']:
        projects = [p.name for p in Project.objects.all()]
    sys.stdout.write('Generating graphs for projects: %s\n' % (', '.join(projects)))
    for project_name in projects:
        logging.info("Doing math for %s" % project_name)
        project = Project.objects.get(name=project_name)
        versions = [v for v in project.projectversion_set.filter(git_sha__isnull=False)] # Rely on natural ordering ("id")
        _versions = []
        metrics = options['metric']
        for v in versions:
            metric_offset_in_versions = 1
            metric_values = []
            for metric_index, metric in enumerate(metrics):
                mv = None
                scale = 1
                if metric == "IC-RFC":
                    scale = .01
                if MetricValue.objects.filter(name=metric, git_sha=v.git_sha).count() == 1:
                    mv = (MetricValue.objects.filter(name=metric, git_sha=v.git_sha).all()[0]).value
                if mv == None:
                    if len(_versions) == 0:
                        metric_values += [0]
                    else:
                        metric_values += [_versions[-1][metric_offset_in_versions + metric_index]]
                else:
                    metric_values += [float(mv) * scale]
            bug_count = v.projectissue_set.count()
            if bug_count == 0 and len(_versions) > 0:
                bug_count = _versions[-1][-1]
            _versions += [[v] + metric_values + [bug_count]]
        versions = _versions
        if len(versions) == 0:
            continue
        logging.info("Generating chart for %s" % project_name)
        fig, ax = pyplot.subplots()
        opts = []
        colors = ['r', 'g', 'y', 'k', 'b']
        handles = []
        for i, m in enumerate(metrics):
            opts += [range(0, len(versions)), [v[i+1] for v in versions], colors[i]]
            handles += [mpatches.Patch(color=colors[i], label=m)]
        opts += [range(0, len(versions)), [v[len(metrics)+1] for v in versions], colors[len(metrics)]]
        handles += [mpatches.Patch(color=colors[len(metrics)], label="Bugs fixed in this version")]
        ax.plot(*opts)
        # , tick_label=
        #logging.info([v[0].name for v in versions])
        version_labels = [v[0].name for v in versions]
        ax.locator_params(tight=True, nbins=len(version_labels))
        ax.set_xticklabels(version_labels, rotation=45.0)
        ax.set_ylabel("%s/# Bugs" % options["metric"])
        ax.set_xlabel("Version")
        ax.set_title(str(project))
        #pyplot.setp(plt, rotation=45, fontsize=8)
        pyplot.legend(handles=handles)
        pyplot.tight_layout()
        pyplot.savefig(os.path.join(options['output'], "%s-metrics_vs_bug_total.png" % (project_name)))
        pyplot.close()
