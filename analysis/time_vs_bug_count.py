#!/usr/bin/env python
""" Downloads a list of projects from the git.apache.org, checks for their
        presence in JIRA, downloads the Git repo in checks to see if it contains
        a pom.xml file. If all of these conditions are met, and the project isn't
        already in the database, inserts the project into the database for analysis
"""
import argparse
import doctest
import os
import sys
import logging

from matplotlib import pyplot

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Generates charts of intervals (release) versus bug count')
    parser.add_argument('project_name', nargs='*', type=str)
    parser.add_argument('--all', action='store_true', dest='all',
        default=False, help='Process all projects')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                action="store_true")
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    options = vars(parser.parse_args())
    if options['verbose']:
        logging.basicConfig(level=logging.DEBUG)
    if options["test"] == True:
        doctest.testmod()
        exit(0)
    sys.path.insert(0, BASE_DIR)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data_gathering.settings")
    try:
        import django
        django.setup()
    except ImportError:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        )
    from django.conf import settings
    from data_gathering.models import Project

    projects = options['project_name']
    if options['all']:
        projects = [p.name for p in Project.objects.all()]
    sys.stdout.write('Generating graphs for projects: %s' % (', '.join(projects)))
    for project_name in projects:
        project = Project.objects.get(name=project_name)
        versions = [(v, v.projectissue_set.filter(issue_type="Bug").count()) for v in project.projectversion_set.all()] # Rely on natural ordering ("id")
        plt = pyplot.bar(range(0, len(versions)), [v[1] for v in versions], .5, tick_label=[v[0].name for v in versions])
        pyplot.setp(plt, rotation=45, fontsize=8)
        pyplot.savefig("%s-time_vs_bug_count.png" % (project_name))
