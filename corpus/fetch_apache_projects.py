#!/usr/bin/env python
""" Downloads a list of projects from the git.apache.org, checks for their
        presence in JIRA, downloads the Git repo in checks to see if it contains
        a pom.xml file. If all of these conditions are met, and the project isn't
        already in the database, inserts the project into the database for analysis
"""
import os
import sys
sys.path.insert(0, ".")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data_gathering.settings")


import django
django.setup()
from django.conf import settings
from data_gathering.models import Project

import argparse
import doctest
import logging
import urllib2
import random
from bs4 import BeautifulSoup
import git

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Downloads Apache projects and puts them into the database')
    parser.add_argument('--url', type=str, dest='url',
        default=os.path.expanduser("https://git.apache.org/"), help='Where to fetch the project list from')
    parser.add_argument('--jira', type=str, dest='jira',
        default=os.path.expanduser("https://issues.apache.org/jira/"), help='Jira base location')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--git-repo-base', type=str, dest='repo_base',
        default=None, help='Where to store the Git repos that must be downloaded')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                action="store_true")
    return parser

def verify_maven_project(git_url, git_root):
    """ Returns true if the project at git_url contains a pom.xml file
    in it's latest master commit
    """
    try:
        git_repo = get_project_repo(git_url, git_root)
        for blob in git_repo.head.commit.tree.blobs:
            if blob.name == "pom.xml" and blob.path == "pom.xml":
                return True
        return False
    except ValueError:
        return False # This means no commits in the repo

def check_jira_access(jira_base, git_url):
    """ Checks to see if a given project (name) is listed in the JIRA API.
    If it is, returns a tuple of (bugtracker_url, bugtracker_api_root)
    """
    url = jira_base + "projects/" + get_project_name(git_url)
    logging.info("Opening %s" % url)
    try:
        req = urllib2.urlopen(url)
    except:
        loggingo.info("Failed to open %s" % url)
        return None
    if req.getcode() == 200:
        return (url, jira_base + "rest/api/")
    return None

def fetch_and_parse_projects_page(page_url):
    """ Fetches the specified page, and parses it looking for a table
    of projects and git repos. Returns a list of the form:
    [name, description, git_url]

    Name is calculated by taking the "Git mirror" column and splitting at the last .
    Other fields are copied verbatim
    """
    res = urllib2.urlopen(page_url)
    page = BeautifulSoup(res.read(), "html.parser")
    for row in page.find_all('tr'):
        if len(row.find_all('th')) != 0:
            continue # Table header row, ignore
        [origin, mirror, description, clone_url, altneratives] = row.find_all('td')
        clone_url = clone_url.a['href']
        yield [get_project_name(clone_url), description.contents, clone_url]

def get_project_name(url):
    """ Given a url, deduces the project name by applying patterns to it
    """
    name = url.split("/")[-1]
    name = ".".join(name.split(".")[0:-1]) # Strip off .git
    name = name.upper()
    return name

def get_project_repo(url, repo_base):
    """ Downloads the project repo to settings.git_repo_base under the name
    of title. If the project already exists, removes is and downloads it again.

    If the source repo is not a Git repo, converts it using an external tool.
    For SVN, we use the git-svn tool
    For CVS, we use cvs2git which will be downloaded if needed
    """
    name = get_project_name(url)
    logging.debug("Downloading source repo for %s from %s" % (name, url))
    destination_repo = os.path.join(repo_base, name)
    if os.path.exists(destination_repo):
        logging.debug("Repo already downloaded, just opening it")
        return git.Repo(destination_repo)
    logging.debug("-> The repo will be saved to %s" % destination_repo)
    return git.Repo.clone_from(url, destination_repo, bare=False)

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    if parser['verbose']:
        logging.basicConfig(level=logging.DEBUG)
    if parser["test"] == True:
        doctest.testmod()
    repo_base = settings.GIT_REPO_BASE
    if parser['repo_base']:
        repo_base = parser['repo_base']
    logging.debug("Using %s as git storage location" % repo_base)
    logging.debug("Using %s as the source for listing projects" % parser['url'])
    logging.debug("Using %s as the JIRA base" % parser['jira'])
    projects = [f for f in fetch_and_parse_projects_page(parser['url'])]
    random.shuffle(projects)
    for project in projects:
        [name, desc, url] =  project
        if len(Project.objects.filter(name=name).all()) != 0:
            logging.info("%s already exists, skipping" % name)
            continue
        maven_repo = verify_maven_project(url, repo_base)
        if not maven_repo:
            logging.warning("%s does not look like a maven repo" % url)
            continue
        jira = check_jira_access(parser['jira'], url)
        if not jira:
            logging.warning("%s does not seem to have Jira access" % url)
            continue
        if maven_repo and jira:
            logging.info("%s has JIRA and pom.xml, saving" % name)
            project = Project(name=name, title=name, description=desc, vcs_type="GIT",
                                vcs_url=url, bugtracker_type="JIRA", bugtracker_url=jira[0],
                                bugtracker_api_root=jira[1])
            project.save()
