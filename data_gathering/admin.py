from django.contrib import admin

from data_gathering.models import Project, ProjectVersion

admin.site.register(Project)
admin.site.register(ProjectVersion)
