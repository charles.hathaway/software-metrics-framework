def match_httpclient(version_string):
    """ Attempts to match version string used by httpclient

    >>> match_httpclient("4.0 Beta 1")
    '4.0-beta1'
    """
    return version_string.replace(" ", "-", 1).lower().replace(" ", "")

def match_spr(version_string):
    """ Matchines a string such as those from Spring Framework

    >>> match_spr("4.3.6")
    'v4.3.6.RELEASE'
    >>> match_spr("5.0 M2")
    'v5.0.0.M2'
    """
    t = version_string.split(" ")
    version = t[0]
    release_type = "RELEASE"
    if len(version.split(".")) != 3:
        version += ".0"
    if len(t) == 2:
        release_type = t[1]
    return "v" + version + "." + release_type

def match_log4j(version_string):
    """ Matches strings using log4j convention

    >>> match_log4j("2.0-beta7")
    'log4j-2.0-beta7'
    >>> match_log4j("2.3")
    'log4j-2.3'
    """
    return "log4j-" + version_string

def match_bookkeeper(version_string):
    """ Matches strings using log4j convention

    >>> match_bookkeeper("2.0.2")
    'release-2.0.2'
    >>> match_bookkeeper("2.3.0")
    'release-2.3.0'
    """
    return "release-" + version_string

def match_beam(version_string):
    """ Matches strings using beam convention

    >>> match_beam("2.0.2")
    'v2.0.2'
    >>> match_beam("2.3.0")
    'v2.3.0'
    """
    return "v" + version_string

def match_isis(version_string):
    """ Matches strings using isis convention

    >>> match_beam("2.0.2")
    'rel/isis-2.0.2'
    >>> match_beam("2.3.0")
    'rel/isis-2.3.0'
    """
    return "rel/isis-" + version_string

def match_crunch(version_string):
    """ Matches strings using isis convention

    >>> match_beam("2.0.2")
    'rel/isis-2.0.2'
    >>> match_beam("2.3.0")
    'rel/isis-2.3.0'
    """
    return "apache-crunch-" + version_string

def match_slider(version_string):
    """ Matches strings using isis convention

    >>> match_beam("2.0.2")
    'rel/isis-2.0.2'
    >>> match_beam("2.3.0")
    'rel/isis-2.3.0'
    """
    parts = version_string.split(" ")
    if len(parts) >= 2:
        version_string = parts[1]
    return "release-" + version_string

def match_slider2(version_string):
    """ Matches strings using isis convention

    >>> match_beam("2.0.2")
    'rel/isis-2.0.2'
    >>> match_beam("2.3.0")
    'rel/isis-2.3.0'
    """
    parts = version_string.split(" ")
    if len(parts) >= 2:
        version_string = parts[1]
    return "release-" + version_string + ".0"

transforms = [match_httpclient, match_spr, match_log4j, match_bookkeeper,
    match_beam, match_isis, match_crunch, match_slider, match_slider2]

# Special transformers
def match_project_name_version(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("LOG4J2", "2.0.3")
    'log4j2-2.0.3'
    >>> match_project_name_version("WINK", "1.3.0")
    'wink-1.3.0'
    """
    return project_name.lower() + "-" + version_string

def jexl_project_name_version(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return "COMMONS_" + project_name + "_" + version_string.replace(".", "_")

def jexl_project_name_version2(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return "COMMONS_" + project_name + "-" + version_string.replace(".", "_")

def jexl_project_name_version3(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return "COMMONS_" + project_name + "_" + version_string.replace(".", "_") + "_0"

def csv_project_name_version(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name + "_" + version_string

def csv_project_name_version2(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name.lower() + "_" + version_string

def SLIDER_project_name_version2(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name.lower() + "-" + version_string

def SLIDER_project_name_version3(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name.lower() + "-" + version_string + ".0-incubating"

def SLIDER_project_name_version4(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name.lower() + "-" + version_string + "-incubating"

def SLIDER_project_name_version5(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name.lower() + "-" + version_string + ".0"

def DBCP_project_name_version2(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return project_name + "_" + version_string.replace(".", "_")

def TEXT_project_name_version2(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return "commons-" + project_name.lower() + "-" + version_string

def GERONIMO_project_name_version2(project_name, version_string):
    """ Given a project name, lowers it and then appends version

    >>> match_project_name_version("JEXL", "2.1.1")
    'log4j2-2.0.3'
    """
    return "geronimo-txmanager-parent-" + version_string

special_transformers = [match_project_name_version, jexl_project_name_version,
    jexl_project_name_version2, jexl_project_name_version3,
    csv_project_name_version, csv_project_name_version2, DBCP_project_name_version2,
    SLIDER_project_name_version2, SLIDER_project_name_version3, SLIDER_project_name_version4,
    SLIDER_project_name_version5, TEXT_project_name_version2, GERONIMO_project_name_version2]
