import subprocess

def execute_command(command, echo=True):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    while(True):
      retcode = p.poll()
      line = p.stdout.readline()
      if echo:
          print "[COMMAND]", line.strip()
      yield line
      if(retcode is not None):
        break
    lines = p.stdout.readlines()
    for line in lines:
        print "[EXECUTE_COMMAND]", line.strip()
        yield line
