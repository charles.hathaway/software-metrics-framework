import json
import urllib2
import os
import shutil

from django.conf import settings
from django.core.exceptions import MultipleObjectsReturned
from django.core.management.base import BaseCommand, CommandError
import git

from data_gathering.models import Project, ProjectVersion, ProjectIssue
from data_gathering.lib.version_transformers import transforms, special_transformers

class UnknownBugTrackerError(Exception):
    pass

class Command(BaseCommand):
    help = 'Fetches bugs and features from a Project bugracker'
    git_tag_transformers = transforms

    def add_arguments(self, parser):
        parser.add_argument('project_name', nargs='*', type=str)
        parser.add_argument('--git-repo-base', type=str, dest='repo_base',
            default=settings.GIT_REPO_BASE, help='Where to store the Git repos that must be downloaded')
        parser.add_argument('--all', action='store_true', dest='all',
            default=False, help='Fetch all projects')
        parser.add_argument('--skip', action='store_true', dest='skip',
            default=False, help='Skip projects already in the database')
        parser.add_argument('--only-versions', action='store_true', dest='versions',
            default=False, help='Only try match versions in git repo with versions in database')
        parser.add_argument('--only-issues', action='store_true', dest='issues',
            default=False, help='Only download issues')
        parser.add_argument('--start-at', type=str, dest='start-at',
            default=None, help='Skip projects until we reach this one')
        parser.add_argument('--start-at-page', type=int, dest='start-at-page',
            default=0, help='The first project should start on this page')

    def handle(self, *args, **options):
        projects = options['project_name']
        hit_start = False
        if options['all']:
            projects = [p.name for p in Project.objects.all()]
        self.stdout.write('Fetching metadata for projects: %s' % (', '.join(projects)))
        for project_name in projects:
            if options['start-at'] != None and not hit_start:
                if project_name != options['start-at']:
                    continue
                hit_start = True
            try:
                project = Project.objects.get(name=project_name)
            except Project.DoesNotExist:
                raise CommandError('Project "%s" does not exist' % project_name)
            if project.projectversion_set.count() > 0 and options['skip']:
                self.stdout.write("Skipping project %s" % project)
                continue
            self.stdout.write('Processing %s...' % project.name)
            if not options['issues']:
                try:
                    self.get_project_versions(project)
                except UnknownBugTrackerError:
                    self.stderr.write(self.style.ERROR("UnknownBugTracker; moving on (%s)" % project))
                    continue
                self.get_project_repo(project, options['repo_base'])
                for version in project.projectversion_set.all():
                    # If we already have a version, skip
                    if version.git_sha != None:
                        continue
                    repo = git.Repo(project.get_repo_local_path(options['repo_base']))
                    matching_sha = self.match_version_name_to_git_commit(project, repo, version)
                    if matching_sha == None:
                        self.stderr.write("Failed to find sha for %s in project %s" % (version, project))
                    else:
                        version.git_sha = matching_sha.commit.hexsha
                        self.stdout.write("Found matching sha for %s; sha = %s" % (version, version.git_sha))
                        version.save()
            if not options['versions']:
                self.get_project_issues(project, options.get("start-at-page", 0))
                options["start-at-page"] = 0;

    def get_project_versions(self, project):
        """ Fetches all issues for the project, and stores them in the
        database. If versions already exist, they are dropped
        """
        self.stdout.write("Fetching projects for %s, type %s" % (project.__str__(), project.bugtracker_type))
        if project.bugtracker_type == "JIRA":
            if project.projectversion_set.count() > 0:
                self.stdout.write("--> Existing versions found; going to not resave matching projects")
            jira_version_api_path = "/2/project/{projectIdOrKey}/versions".format(projectIdOrKey = project.name)
            if project.bugtracker_api_root[-1] == '/':
                project.bugtracker_api_root = project.bugtracker_api_root[:-1]
            response = self.fetch_api_field(project.bugtracker_api_root + jira_version_api_path)
            response = [v for v in response][0]
            for version in response:
                if project.projectversion_set.filter(name=version['name']).count() == 0:
                    self.stdout.write("Saving %s version %s" % (project.__str__(), version['name']))
                    temp = ProjectVersion(project=project, self_url=version['self'],
                        self_id=version['id'], name=version['name'])
                    temp.save()
        else:
            raise UnknownBugTrackerError("Unknown bugtracker_type")

    def get_project_repo(self, project, repo_base):
        """ Downloads the project repo to settings.git_repo_base under the name
        of title. If the project already exists, simply reuses it.

        If the source repo is not a Git repo, converts it using an external tool.
        For SVN, we use the git-svn tool
        For CVS, we use cvs2git which will be downloaded if needed
        """
        self.stdout.write("Downloading source repo for %s from %s" % (project.__str__(), project.vcs_url))
        source_repo = project.vcs_url
        destination_repo = project.get_repo_local_path(repo_base)
        self.stdout.write("-> The repo will be saved to %s" % destination_repo)
        if os.path.exists(destination_repo):
            if os.path.isdir(destination_repo):
                self.stdout.write("-> Found older directory, using")
                return
            else:
                self.stderr.write("Found file in location where folder was expected; please clean it up")
                raise Exception("File found at %s, expected directory" % destination_repo)
        if project.vcs_type == "SVN":
            pass
        elif project.vcs_type == "GIT":
            source_repo = project.vcs_url
        else:
            raise Exception("Unknown vcs_type")
        git.Repo.clone_from(source_repo, destination_repo, bare=False)

    def get_project_issues(self, project, start_at=0):
        """ Given a project, search for the last downloaded issue and resumes
        downloading from that point
        """
        self.stdout.write("Downloading issues for %s" % (project))
        if project.bugtracker_type == "JIRA":
            jira_bug_list_api_path = "/2/search?jql=project='{project_name}'".format(project_name=project.name)
            if project.bugtracker_api_root[-1] == '/':
                project.bugtracker_api_root = project.bugtracker_api_root[:-1]
            bug_list = self.fetch_api_field(project.bugtracker_api_root + jira_bug_list_api_path, field="issues", start_at=start_at)
            for bug in bug_list:
                issue = ProjectIssue(name=bug["key"], created=bug["fields"]["created"], updated=bug["fields"]["updated"],
                                status=bug["fields"]["status"]["name"], issue_type=bug["fields"]["issuetype"]["name"])
                self.stdout.write("-> Saving issue %s" % issue.name)
                issue.save()
                for version in bug["fields"]["fixVersions"]:
                    try:
                        version = ProjectVersion.objects.get(name=version["name"], project=project)
                    except MultipleObjectsReturned:
                        self.stderr.write("Found multiples versions for %s %s" % (project, version["name"]))
                        continue
                    except:
                        self.stderr.write("Failed to find referenced version in %s" % bug["self"])
                        self.stderr.write("name=%s project.id=%s" % (version["name"], project.id))
                        continue
                    if not issue.fix_version.filter(id=version.id).exists():
                        issue.fix_version.add(version)
                issue.save()
        else:
            raise UnknownBugTrackerError("Unknown bugtracker_type")

    def fetch_api_field(self, url, format="json", field=None, start_at=0):
        """ Fetches a url in format format (default json) and returns a Python
        representation of that object; this may be blocking, if the API is rate limited

        If field is not None, will return that field in the object and deal with pagination
        """
        max_results = 100
        o_url = url
        while True:
            url = o_url
            if "?" not in url:
                url += "?"
            else:
                url += "&"
            url += "startAt=%s&maxResults=%s" % (start_at, max_results)
            self.stdout.write("Fetching URL %s" % url)
            object = None
            if format == "json":
                url_object = urllib2.urlopen(url)
                object = json.load(url_object)
            else:
                raise Exception("Unknown format")
            if object == None:
                raise Exception("Failed to fetch URL %s" % url)
            if field == None:
                yield object
                break
            max_results = object["maxResults"]
            start_at += max_results
            for o in object[field]:
                yield o
            if start_at > object["total"]:
                break

    def match_version_name_to_git_commit(self, project, repo, version):
        """ Given a Git repo object and a version object, attempts to find
        the a matching git tag/branch for the version and records the sha
        """
        self.stdout.write("Attempting to match version (%s) to a sha in %s" % (version, repo))
        if not hasattr(self, '_git_commit_cache'):
            self._git_commit_cache = {}
        tags = []
        if project.name not in self._git_commit_cache:
            self._git_commit_cache[project.name] = [t.name for t in repo.tags] + [t.name for t in repo.branches]
            print self._git_commit_cache
        tags = self._git_commit_cache[project.name]
        # First, assemble a list of possible names. This is done by invoking
        #  many different "transformers" on the given name
        self.stdout.write("Trying %s transformers..." % len(self.git_tag_transformers))
        possible_tags = []
        for part in version.name.split(" "):
            part = part.strip()
            possible_tags += [transformer(part) for transformer in self.git_tag_transformers]
            possible_tags += [t(project.name, part) for t in special_transformers]
        self.stdout.write(", ".join(possible_tags))
        # Check for overlaps
        for tag in possible_tags:
            if tag in tags:
                if tag in repo.tags:
                    return repo.tags[tag]
                elif tag in repo.branches:
                    return repo.branches[tag]
                raise Exception("Tag/branch from unknown source")
        return None
