import json
import urllib2
import os
import shutil
import subprocess
import re

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
import git

from data_gathering.models import Project, ProjectVersion, MetricValue
from data_gathering.lib.version_transformers import transforms

class NoShaForVersionError(Exception):
    pass

class FailedToCompileException(Exception):
    pass

def execute_command(command):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    while(True):
      retcode = p.poll()
      line = p.stdout.readline()
      print "[EXECUTE_COMMAND]", line.strip()
      yield line
      if(retcode is not None):
        break
    lines = p.stdout.readlines()
    for line in lines:
        print "[EXECUTE_COMMAND]", line.strip()
        yield line

class Command(BaseCommand):
    help = """ Runs the script specified as an argument, passing in the path to MVN
    project as an argument

The script should can report metric values by printing to STDOUT like this:

#>> METRIC_NAME=METRIC_VALUE

The output is grepped for #>>, then the values split and stored

If the script runs longer than --timeout (default, 5 minutes) it gets killed
"""
    version_metric_pattern = re.compile(r'(?P<metric_name>[^=:]+)=(?P<metric_value>\d+(\.\d+)*)')
    file_metric_pattern = re.compile(r'(?P<metric_name>[^=:]+):(?P<file_name>[^=:]+)=(?P<metric_value>\d+(\.\d+)*)')
    line_metric_pattern = re.compile(r'(?P<metric_name>[^=:]+):(?P<file_name>[^=:]+):(?P<line_number>[^=:]+)=(?P<metric_value>\d+(\.\d+)*)')
    def add_arguments(self, parser):
        parser.add_argument('shell_script', nargs='+', type=str, help="A script to run")
        parser.add_argument('--git-repo-base', type=str, dest='repo_base',
            default=settings.GIT_REPO_BASE, help='Where to store the Git repos that must be downloaded')
        parser.add_argument('--no-compile', action='store_true', dest='compile',
            default=False, help='Do not compile projects before calling the metrics')
        parser.add_argument('--project', type=str, dest='project', default=None,
            help='Which project to run on; if omitted, runs on all projects')
        parser.add_argument('--start-at', type=str, dest='start-at',
            default=None, help='Skip projects until we reach this one')

    def handle(self, *args, **options):
        projects = Project.objects.all()
        hit_start = False
        if options["project"] != None:
            projects = [Project.objects.get(name=options["project"])]
        for project in projects:
            if options['start-at'] != None and not hit_start:
                if project.name != options['start-at']:
                    continue
                hit_start = True
            self.stdout.write("Starting work on project %s" % project)
            # Check out a version, compile it
            for version in project.projectversion_set.all():
                self.stdout.write("Checking out version and building %s..." % (version))
                try:
                    self.checkout(project, version, options['repo_base'])
                    if not options['compile']:
                        self.build(project, options['repo_base'])
                    else:
                        self.stdout.write("Not building %s" % version)
                except NoShaForVersionError as e:
                    continue
                except FailedToCompileException as e:
                    continue
                for script in options['shell_script']:
                    stdout = ""
                    stderr = ""
                    project_path = project.get_repo_local_path(options['repo_base'])
                    self.stdout.write("Running script %s on project %s" % (script, project_path))
                    stdout = execute_command("%s %s" % (script, project_path))
                    self.stdout.write("Script complete; parsing output")
                    for line in stdout:
                        if line.startswith("#>>"):
                            metric_name = metric_value = file_name = line_number = None
                            line = line[len('#>>'):].strip()
                            match_version = Command.version_metric_pattern.match(line)
                            match_file = Command.file_metric_pattern.match(line)
                            match_line = Command.line_metric_pattern.match(line)
                            if match_version:
                                metric_name = match_version.group('metric_name')
                                metric_value = match_version.group('metric_value')
                            elif match_file:
                                metric_name = match_file.group('metric_name')
                                metric_value = match_file.group('metric_value')
                                file_name = match_file.group('file_name')
                            elif match_line:
                                metric_name = match_line.group('metric_name')
                                metric_value = match_line.group('metric_value')
                                file_name = match_line.group('file_name')
                                line_number = match_line.group('line_number')
                            try:
                                metric_name = metric_name.strip()
                                metric_value = metric_value.strip()
                                if file_name:
                                    file_name = file_name.strip()
                                if line_number:
                                    line_number = line_number.strip()
                                self.stdout.write("Found metric %s with value %s; saving" % (metric_name, metric_value))
                                if MetricValue.objects.filter(name=metric_name, git_sha=version.git_sha, file=file_name, line_number=line_number).count() != 0:
                                    self.stdout.write("-> Metric already in database, deleting and putting new value")
                                    MetricValue.objects.filter(name=metric_name, git_sha=version.git_sha, file=file_name, line_number=line_number).delete()
                                mv = MetricValue(project=project, name=metric_name,
                                        value=metric_value, git_sha=version.git_sha,
                                        file=file_name, line_number=line_number)
                                mv.save()
                            except e:
                                print str(e)
                                self.stderr.write(self.style.ERROR("Problem parsing \"%s\"" % line))

    def checkout(self, project, version, repo_base):
        if version.git_sha == None:
            self.stdout.write(self.style.ERROR("No sha for version %s; skipping" % version))
            raise NoShaForVersionError()
        git_path = project.get_repo_local_path(repo_base)
        repo = git.Repo(git_path)
        repo.git.clean('-xdf')
        repo.head.reference = repo.commit(version.git_sha)
        repo.head.reset(index=True, working_tree=True)

    def build(self, project, repo_base):
        git_path = project.get_repo_local_path(repo_base)
        #Try building
        cur_dir = os.getcwd()
        try:
            os.chdir(git_path)
            self.stdout.write("Compiling at %s" % os.getcwd())
            result = subprocess.call(["mvn compile -DskipTests"], shell=True)
            if result != 0:
                raise FailedToCompileException()
        finally:
            os.chdir(cur_dir)
