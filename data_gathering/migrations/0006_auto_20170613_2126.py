# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-13 21:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_gathering', '0005_auto_20170531_0009'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectissue',
            name='created',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='projectissue',
            name='fix_version',
            field=models.ManyToManyField(to='data_gathering.ProjectVersion'),
        ),
        migrations.AddField(
            model_name='projectissue',
            name='name',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectissue',
            name='status',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='projectissue',
            name='updated',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
