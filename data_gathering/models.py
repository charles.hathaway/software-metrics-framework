import os

from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    description = models.TextField()
    homepage = models.CharField(max_length=255)

    vcs_type = models.CharField(max_length=10, choices=(
        ("GIT", "Git"),
        ("SVN", "Subversion"),
    ))
    vcs_url = models.CharField(max_length=255)

    bugtracker_type = models.CharField(max_length=10, choices=(
        ("JIRA", "Jira"),
        ("GHUB", "Github"),
    ))
    bugtracker_url = models.URLField()
    bugtracker_api_root = models.URLField()

    prebuild_script = models.CharField(max_length=255, null=True, blank=True)
    postbuild_script = models.CharField(max_length=255, null=True, blank=True)
    cleanup_script = models.CharField(max_length=255, null=True, blank=True)


    def get_repo_local_path(self, repo_base):
        """ Returns a string which will be the location of the git repo on disk,
          if it exists
        """
        destination_repo = os.path.join(repo_base, self.name)
        return destination_repo

    def __str__(self):
        return "%s (%s)" % (self.title, self.name)

class ProjectIssue(models.Model):
    name = models.CharField(max_length=255)
    fix_version = models.ManyToManyField('data_gathering.ProjectVersion')
    created = models.DateTimeField(null=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=255, null=True, blank=True)
    issue_type = models.CharField(max_length=255)

class ProjectVersion(models.Model):
    project = models.ForeignKey('data_gathering.Project')
    self_url = models.URLField()
    self_id = models.CharField(max_length=255)
    description = models.TextField()
    name = models.CharField(max_length=255)

    git_tag = models.CharField(max_length=255, null=True)
    git_sha = models.CharField(max_length=40, null=True)

    def __str__(self):
        return "<%s>" % (self.name)

class MetricValue(models.Model):
    name = models.CharField(max_length=255)
    value = models.DecimalField(max_digits=19, decimal_places=5)

    project = models.ForeignKey('data_gathering.Project')
    git_sha = models.CharField(max_length=40, null=True)

    # Options to support a class/file, line number
    file = models.CharField(max_length=255, null=True, blank=True)
    line_number = models.IntegerField(null=True, blank=True)
