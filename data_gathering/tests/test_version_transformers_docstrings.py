import unittest
import doctest
import data_gathering.lib.version_transformers

def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(data_gathering.lib.version_transformers))
    return tests
