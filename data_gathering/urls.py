"""data_gathering URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.views.static import serve

from data_gathering.views import ProjectList, ProjectDetail

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', ProjectList.as_view(), name='project_list'),
    url(r'^files/', include('media_views.urls')),
    url(r'^(?P<pk>\d+)/$', ProjectDetail.as_view(), name='project_detail'),
    url(r'^static/(?P<path>.*)$', serve, {
	'document_root': settings.STATIC_ROOT,
	'show_indexes': True
    }),
]
