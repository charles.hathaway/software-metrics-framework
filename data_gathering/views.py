from django.views.generic import ListView, DetailView

from data_gathering.models import Project

class ProjectList(ListView):
    model = Project

class ProjectDetail(DetailView):
    model = Project
