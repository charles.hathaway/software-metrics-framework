#!/usr/bin/env python
""" Given a project in the Maven repository, calculates the depth
of the inheritance tree of that project.

To save time on future iterations, this script serializes the parent_tree to
a file located in $TEMP/ic_dit.pickle. If $TEMP is undefined or empty, we
assume it's /tmp/ (or C:/tmp/)

Environmental variables:
 TEMP - a temporary workspace, assumed /tmp/ if not provided
 MAVEN_CACHE - directory containing all maven dependencies for an already
    built project. If blank, assumed to be ~/.m2/.
"""
import argparse
import doctest
import pickle
import os
import sys
import subprocess
import Queue
import fcntl
import signal
import time

from data_gathering.library.commands import execute_command

def get_depends_list(path_to_pom):
    """ Returns a list of (name, version) that represent the parents of this pom
    """
    lines = execute_command("mvn -f %s dependency:tree" % path_to_pom, echo=False)
    state = "passing"
    out = 0
    for line in lines:
        line = line[len("[INFO]"):].strip()
        print "[%s]" % state,  line
        if state == "parsing_deps":
            if line == "":
                state = "passing"
            elif line == "none":
                state = "passing"
            else:
                depth = 0
                while depth < len(line) and not line[depth].isalpha():
                    depth += 1
                if depth == len(line):
                    state = "passing"
                    continue
                level = depth / 3
                if level > out:
                    out = level
        elif state == "passing" and line.startswith("--- maven-dependency-plugin"):
            state = "parsing_deps"
        else:
            pass
    return out

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Calculate the Interactive Complexity metric DIT for a project')
    parser.add_argument('--temp-file', type=str, dest='temp_file',
        default="/tmp/ic_dit.pickle", help='Where to store temporary pickle (cache)')
    parser.add_argument('--no-cache', action='store_true', dest='no_cache',
        default=False, help='If true, we don\'t use the cache')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--pom', action='store_true', dest='pom',
        default=False, help='If true, the last parameter is a direct POM')
    parser.add_argument('--mvn-cache', type=str, dest='maven_cache',
        default=os.path.expanduser("~/.m2/"), help='Directory where the local maven cache is')
    parser.add_argument('PROJECT_ROOT', type=str,
                        help='location of the project directory')
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    ic_loops = 0
    if parser["test"] == True:
        doctest.testmod()
    # Start fetching tree information
    print "Scanning POM..."
    if os.path.isdir(parser["PROJECT_ROOT"]):
        parser["PROJECT_ROOT"] = os.path.join(parser["PROJECT_ROOT"], "pom.xml")
    result = get_depends_list(parser["PROJECT_ROOT"])
    print "#>> IC-DIT=%s" % result
