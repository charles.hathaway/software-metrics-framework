#!/usr/bin/env python
""" Given a project in the Maven repository, calculates the LCOM1
measure of that project.
"""
import argparse
import doctest
import pickle
import os
import sys
import subprocess
import Queue
import fcntl
import signal
import time

from data_gathering.library.commands import execute_command

# Serialize parent_tree
tree_file_d = None
tmp_file_name = "/tmp/ic-dit.pickle"
parent_tree = {
    "version": 1,
    "objects": {
    }
}

visited_paths = []

def get_depends_list(path_to_pom):
    """ Returns a list of (name, version) that represent the parents of this pom
    """
    lines = execute_command("mvn -f %s dependency:analyze-only" % path_to_pom, echo=False)
    state = "passing"
    out = 0
    for line in lines:
        line = line.split("]", 1)
        if len(line) < 2:
            state = "passing"
            continue
        line = line[1].strip()
        print "[%s]" % state,  line
        if state == "parsing_deps":
            if line == "":
                state = "passing"
            elif line == "none":
                state = "passing"
            else:
                out += 1
        elif state == "passing" and line.startswith("Unused declared dependencies found:"):
            state = "parsing_deps"
        else:
            pass
    return out

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Calculate the Interactive Complexity metric LCOM1 for a project')
    parser.add_argument('--temp-file', type=str, dest='temp_file',
        default="/tmp/ic_dit.pickle", help='Where to store temporary pickle (cache)')
    parser.add_argument('--no-cache', action='store_true', dest='no_cache',
        default=False, help='If true, we don\'t use the cache')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--pom', action='store_true', dest='pom',
        default=False, help='If true, the last parameter is a direct POM')
    parser.add_argument('--mvn-cache', type=str, dest='maven_cache',
        default=os.path.expanduser("~/.m2/"), help='Directory where the local maven cache is')
    parser.add_argument('PROJECT_ROOT', type=str,
                        help='location of the project directory')
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    ic_loops = 0
    if parser["test"] == True:
        doctest.testmod()
    # Start fetching tree information
    print "Scanning POM..."
    if os.path.isdir(parser["PROJECT_ROOT"]):
        parser["PROJECT_ROOT"] = os.path.join(parser["PROJECT_ROOT"], "pom.xml")
    result = get_depends_list(parser["PROJECT_ROOT"])
    print "#>> IC-LCOM1=%s" % result
