#!/usr/bin/env python
""" Scan mvnrepository.com for usage stats
"""
import os
import sys
sys.path.insert(0, ".")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data_gathering.settings")


import django
django.setup()
from django.conf import settings
from data_gathering.models import Project

import argparse
import doctest
import logging
import urllib2
import random
import subprocess
from bs4 import BeautifulSoup
import git

from data_gathering.library.commands import execute_command

def get_attribute(path_to_pom, attribute):
    lines = execute_command("mvn -f %s org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.%s" %
        (path_to_pom, attribute))
    lines = [l for l in lines]
    for line in reversed(lines):
        if not line.startswith("[INFO]") and not line.startswith("[WARNING]") \
            and not line.startswith("Downloading") and line.strip() != "":
            return line


def get_arg_parser():
    parser = argparse.ArgumentParser(description='Downloads Apache projects and puts them into the database')
    parser.add_argument('--url', type=str, dest='url',
            default="https://mvnrepository.com/", help='Where to fetch stats from')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                action="store_true")
    parser.add_argument('PROJECT_ROOT', type=str,
                help='location of the project directory')

    return parser

def check_jira_access(jira_base, git_url):
    """ Checks to see if a given project (name) is listed in the JIRA API.
    If it is, returns a tuple of (bugtracker_url, bugtracker_api_root)
    """
    url = jira_base + "projects/" + get_project_name(git_url)
    logging.info("Opening %s" % url)
    try:
        req = urllib2.urlopen(url)
    except:
        loggingo.info("Failed to open %s" % url)
        return None
    if req.getcode() == 200:
        return (url, jira_base + "rest/api/")
    return None

cache = {}

def fetch_noc(page_url, version):
    """ Fetches the specified page, and parses it looking for a table
    of projects and git repos. Returns a list of the form:
    [name, description, git_url]

    Name is calculated by taking the "Git mirror" column and splitting at the last .
    Other fields are copied verbatim
    """
    if page_url in cache and version in cache[page_url]:
        return cache[page_url][version]
    res = urllib2.urlopen(page_url)
    page = BeautifulSoup(res.read(), "html.parser")
    for row in page.find_all('tr'):
        cells = row.find_all('td')
        if len(cells) != 5 and len(cells) != 4:
            continue
        t_version = usages = ""
        if len(cells) == 5:
            [junk1, t_version, junk2, usages, junk3] = cells
        elif len(cells) == 4:
            [t_version, junk2, usages, junk3] = cells
        usages = usages.find_all('a')
        print usages
        if len(usages) == 0:
            usages = 0
        else:
            usages = usages[0].text
        t_version = t_version.find_all('a')[0].text
        if page_url not in cache:
            cache[page_url] = {}
        if t_version not in cache[page_url]:
            cache[page_url][t_version] = 0
        print usages
        cache[page_url][t_version] += int(usages)
    if page_url in cache and version in cache[page_url]:
        return cache[page_url][version]
    return False

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    logging.basicConfig(level=logging.DEBUG)
    if parser['verbose']:
        logging.basicConfig(level=logging.DEBUG)
    if parser["test"] == True:
        doctest.testmod()
        exit(0)
    if os.path.isdir(parser["PROJECT_ROOT"]):
        parser["PROJECT_ROOT"] = os.path.join(parser["PROJECT_ROOT"], "pom.xml")
    logging.debug("Using %s as the source for getting stats" % parser['url'])
    # Get the project groupId, artifactId, and version
    groupId = get_attribute(parser["PROJECT_ROOT"], "groupId").strip()
    artifactId = get_attribute(parser["PROJECT_ROOT"], "artifactId").strip()
    version = get_attribute(parser["PROJECT_ROOT"], "version").strip()
    target = "{base_url}/artifact/{groupId}/{artifactId}".format(base_url=parser['url'],
	groupId=groupId, artifactId=artifactId)
    print "Getting values from", target
    print "Getting values from %s for %s" % (target, version)
    value = fetch_noc(target, version)
    if value is False:
        # A common pattern is to have "core" project; try that
        target = "{base_url}/artifact/{groupId}/{artifactId}-core".format(base_url=parser['url'],
            groupId=groupId, artifactId=artifactId)
        print "Getting values from", target
        logging.debug("Getting values from %s", target)
        value = fetch_noc(target, version)
    if value is not False:
        print "#>> IC-NOC=%s" % value
    else:
        print "Couldn't find IC-NOC value for project"
