#!/usr/bin/env python
""" Given a project in the Maven repository, calculates the depth
of the inheritance tree of that project.
"""
import argparse
import doctest
import pickle
import os
import sys
import subprocess
import Queue
import fcntl
import signal
import time
import pipes

from data_gathering.library.commands import execute_command

# Serialize parent_tree
tree_file_d = None
tmp_file_name = "/tmp/ic-dit.pickle"
parent_tree = {
    "version": 1,
    "objects": {
    }
}

visited_paths = []

def get_class_files(path_to_pom):
    files = [line for line in execute_command("find %s -name *.class" % dir)]
    return files

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Calculate the Interactive Complexity metric RFC for a project')
    parser.add_argument('--temp-file', type=str, dest='temp_file',
        default="/tmp/ic_dit.pickle", help='Where to store temporary pickle (cache)')
    parser.add_argument('--no-cache', action='store_true', dest='no_cache',
        default=False, help='If true, we don\'t use the cache')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--pom', action='store_true', dest='pom',
        default=False, help='If true, the last parameter is a direct POM')
    parser.add_argument('--mvn-cache', type=str, dest='maven_cache',
        default=os.path.expanduser("~/.m2/"), help='Directory where the local maven cache is')
    parser.add_argument('PROJECT_ROOT', type=str,
                        help='location of the project directory')
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    total = 0
    if parser["test"] == True:
        doctest.testmod()
    # Start fetching tree information
    print "Scanning POM..."
    if os.path.isdir(parser["PROJECT_ROOT"]):
        parser["PROJECT_ROOT"] = os.path.join(parser["PROJECT_ROOT"], "pom.xml")
    dir = os.path.dirname(parser["PROJECT_ROOT"])
    class_files = get_class_files(dir)
    for c in class_files:
        c = c.strip()
        result = execute_command("javap %s" % (pipes.quote(c)))
        count = 0
        for r in result:
            r = r.strip()
            if r.startswith("public"):
                count += 1
        if count > 0:
            total += count - 1 # minus one for class declaration
        print "%s: %s" % (c, count)
    print "#>> IC-RFC=%s" % total
