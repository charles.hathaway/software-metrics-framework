#!/usr/bin/env python
""" Given a project in the Maven repository, calculates the depth
of the inheritance tree of that project.

To save time on future iterations, this script serializes the parent_tree to
a file located in $TEMP/ic_dit.pickle. If $TEMP is undefined or empty, we
assume it's /tmp/ (or C:/tmp/)

Environmental variables:
 TEMP - a temporary workspace, assumed /tmp/ if not provided
 MAVEN_CACHE - directory containing all maven dependencies for an already
    built project. If blank, assumed to be ~/.m2/.
"""
import argparse
import doctest
import pickle
import os
import sys
import subprocess
import Queue
import fcntl
import signal
import time

from data_gathering.library.commands import execute_command

def get_depends_list(path_to_pom):
    """ Returns a list of (name, version) that represent the parents of this pom
    """
    lines = run_command("mvn -f %s dependency:list" % path_to_pom)
    state = "passing"
    out = []
    for line in lines:
        print state, line
        line = line[len("[INFO]"):].strip()
        if state == "parsing_deps":
            if line == "":
                state = "passing"
            elif line == "none":
                state = "passing"
            else:
                # commons-codec:commons-codec:jar:1.9:compile
                parts = line.split(":")
                if len(parts) == 5:
                    [groupId, artifactId, junk, version, junk2] = parts
                elif len(parts) == 6:
                    [groupId, artifactId, junk, test, version, junk2] = parts
                out += [(groupId,  artifactId)]
        elif state == "passing" and line == "The following files have been resolved:":
            state = "parsing_deps"
        else:
            pass
    unique_projects = list(set(out))
    return unique_projects

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Calculate the Interactive Complexity metric DIT for a project')
    parser.add_argument('--temp-file', type=str, dest='temp_file',
        default="/tmp/ic_dit.pickle", help='Where to store temporary pickle (cache)')
    parser.add_argument('--no-cache', action='store_true', dest='no_cache',
        default=False, help='If true, we don\'t use the cache')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--pom', action='store_true', dest='pom',
        default=False, help='If true, the last parameter is a direct POM')
    parser.add_argument('--mvn-cache', type=str, dest='maven_cache',
        default=os.path.expanduser("~/.m2/"), help='Directory where the local maven cache is')
    parser.add_argument('PROJECT_ROOT', type=str,
                        help='location of the project directory')
    return parser

def get_project_pom_path(mvn_cache_path, name, version):
    """ Given a name ("junit.junit") returns the path

    >>> get_project_pom_path("~/.m2/", ("junit", "junit"), "3.8.1")
    '~/.m2/repository/junit/junit/3.8.1/junit-3.8.1.pom'
    """
    (groupId, artifactId) = name
    name_parts = groupId.split(".")
    path = mvn_cache_path + "repository/"
    for p in name_parts:
        path += p + "/"
    path += artifactId + "/"
    path += version + "/"
    path += artifactId + "-" + version + ".pom"
    return path

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    # Start fetching tree information
    print "Scanning POM..."
    # If the given PROJECT_ROOT is a directory, make it a pom.xml file
    if os.path.isdir(parser["PROJECT_ROOT"]):
        parser["PROJECT_ROOT"] = os.path.join(parser["PROJECT_ROOT"], "pom.xml")
    parents = get_depends_list(parser["PROJECT_ROOT"])
    print "#>> IC-WMC=%s" % len(parents)
