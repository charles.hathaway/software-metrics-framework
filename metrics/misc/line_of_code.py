#!/usr/bin/env python
""" Given a project in the Maven repository, calculates the depth
of the inheritance tree of that project.

To save time on future iterations, this script serializes the parent_tree to
a file located in $TEMP/ic_dit.pickle. If $TEMP is undefined or empty, we
assume it's /tmp/ (or C:/tmp/)

Environmental variables:
 TEMP - a temporary workspace, assumed /tmp/ if not provided
 MAVEN_CACHE - directory containing all maven dependencies for an already
    built project. If blank, assumed to be ~/.m2/.
"""
import argparse
import doctest
import pickle
import os
import sys
import subprocess
import Queue
import fcntl
import signal
import time
import json

from data_gathering.library.commands import execute_command

def get_arg_parser():
    parser = argparse.ArgumentParser(description='Calculate the Interactive Complexity metric DIT for a project')
    parser.add_argument('--temp-file', type=str, dest='temp_file',
        default="/tmp/ic_dit.pickle", help='Where to store temporary pickle (cache)')
    parser.add_argument('--no-cache', action='store_true', dest='no_cache',
        default=False, help='If true, we don\'t use the cache')
    parser.add_argument('--test', action='store_true', dest='test',
        default=False, help='Runs the built in tests')
    parser.add_argument('--pom', action='store_true', dest='pom',
        default=False, help='If true, the last parameter is a direct POM')
    parser.add_argument('--mvn-cache', type=str, dest='maven_cache',
        default=os.path.expanduser("~/.m2/"), help='Directory where the local maven cache is')
    parser.add_argument('PROJECT_ROOT', type=str,
                        help='location of the project directory')
    return parser

if __name__ == "__main__":
    parser = get_arg_parser()
    parser = vars(parser.parse_args())
    ic_loops = 0
    if parser["test"] == True:
        doctest.testmod()
    # Call cloc to calculate lines of code
    path = parser['PROJECT_ROOT']
    if not os.path.isdir(path):
        path = os.path.dirname(parser['PROJECT_ROOT'])
    os.chdir(path)
    result = execute_command("cloc --json .")
    result = ' '.join(result)
    result = json.loads(result)
    print "#>> LOC=%s" % result['SUM']['code']
